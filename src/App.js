import './App.css';
import Brand from './Brand.json'
import {useState} from 'react'

function App() {
  const [search, setSearch] = useState('')
 
  return (
    
    <div className="App">
      <h1>Search Bar</h1>
      <input 
        type="text" 
        placeholder="Searh..." 
        onChange={event => {
          setSearch(event.target.value);
          }} 
      />
      
       
       {Brand.filter((value) => {
        
        if(search === ""){
            return value
         }else if (value.name.toLowerCase().includes(search.toLowerCase())){
          return value
         }         
        
    }).map((value, key)=>{
        return(
        <div className="brand" key={key}>
              <p>{value.name}</p>
        </div>
        );
      })}
    </div>
  );
}

export default App;
